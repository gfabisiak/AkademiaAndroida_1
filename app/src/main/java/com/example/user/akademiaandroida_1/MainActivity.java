package com.example.user.akademiaandroida_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final static String goalReachedText = "Cel osiągnięty!";

    TextView status_tv;
    Button button;
    TextView number_tv;

    private Integer count;
    private String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        status_tv = findViewById(R.id.textStatus);
        button = findViewById(R.id.button);
        number_tv = findViewById(R.id.number);

        if(savedInstanceState != null){
            count = savedInstanceState.getInt("savedNumber");
            status = savedInstanceState.getString("savedStatus");
            status_tv.setText(status);
            number_tv.setText(count.toString());
        }
        else {
            count = 0;
            number_tv.setText(count.toString());
            status_tv.setText("");
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String countString = number_tv.getText().toString();
                //count = Integer.parseInt(countString);
                count++;
                number_tv.setText(count.toString());

                if (count == 10)
                    ShowToast(v);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("savedNumber", count);
        outState.putString("savedStatus", status);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("savedNumber");
        status = savedInstanceState.getString("savedStatus");
    }

    private void ShowToast(View v) {
        status_tv = findViewById(R.id.textStatus);
        status = goalReachedText;
        status_tv.setText(status);
        Toast.makeText(this, goalReachedText, Toast.LENGTH_LONG).show();
    }
}
